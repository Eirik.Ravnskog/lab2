package INF101.lab2.pokemon;
import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    // Create field variables here:
    String name; //navnet til Pokemon'en
    int healthPoints; //hvor mange liv Pokemon'en har 
    int maxHealthPoints; //Hvor mange health points Pokémon'en kan maksimalt ha
    int strength; //hvor sterk Pokemon'en er 

    ///// Oppgave 1b
    // Create a constructor here:
    public Pokemon(String name, int healthPoints, int strength) {
        this.name = name; 
        this.healthPoints = healthPoints;
        this.maxHealthPoints = healthPoints; 
        this.strength = strength; 
    }

    ///// Oppgave 2
	/**
     * Get name of the pokémon
     * @return name of pokémon
     */
    String getName() {
        return name; 
    }

    /**
     * Get strength of the pokémon
     * @return strength of pokémon
     */
    int getStrength() {
        return strength;
    }

    /**
     * Get current health points of pokémon
     * @return current HP of pokémon
     */
    int getCurrentHP() {
        return healthPoints; 
    }
    
    /**
     * Get maximum health points of pokémon
     * @return max HP of pokémon
     */
    int getMaxHP() {
        return maxHealthPoints; 
    }

    /**
     * Check if the pokémon is alive. 
     * A pokemon is alive if current HP is higher than 0
     * @return true if current HP > 0, false if not
     */
    boolean isAlive() {
        return getCurrentHP() > 0; 
    }

    
    ///// Oppgave 4
    /**
     * Damage the pokémon. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     *
     * It should not be possible to deal negative damage, i.e. increase the number of health points.
     *
     * The method should print how much HP the pokemon is left with.
     *
     * @param damageTaken
     */
    void damage(int damageTaken) {
        healthPoints = getCurrentHP();
      
        if (healthPoints < 0) {
            System.out.println("Invalid HP value. Cannot have less than 0 healthpoints.");
            return; 
        }
        
        if (damageTaken < 0) {
            System.out.println("Invalid damage value. Cannot have negative damage.");
            return; 
        }
    
        if (healthPoints < damageTaken) {
            healthPoints = 0;
        } else {
            healthPoints -= damageTaken;
        }
        System.out.println(getName() + " takes " + damageTaken + " damage and is left with " + getCurrentHP() + "/" + getMaxHP() + " HP)");  

          
        
    }
    

    ///// Oppgave 5
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * 
     * If <code>target</code> has 0 HP then print that it was defeated.
     * 
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        
        Random rand = new Random();

        int damageInflicted = rand.nextInt(getStrength()); 
        
        System.out.println(getName() + " attacks " + target.getName()); //eller this.name 
        target.damage(damageInflicted);
        

        if (!target.isAlive()) {
            System.out.println(target.getName() + " is defeated by " + getName());
        }
    }

    ///// Oppgave 3
    @Override
    public String toString() {

        return getName() + " HP: (" + getCurrentHP() + "/" + getMaxHP() + ") STR: " + getStrength();
    }

}
