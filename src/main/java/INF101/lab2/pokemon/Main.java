package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;


    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated

        pokemon1 = new Pokemon("Pikachu", 35, 55);
        pokemon2 = new Pokemon("Oddish", 45, 50);


        pokemon1.toString();
        pokemon2.toString();

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (!pokemon2.isAlive()) {
                break; 
            }
            pokemon2.attack(pokemon1);
            
        }
    }
}
